# Formulary HB3
HB9Cl HB3/Novice amateur radio license formulary

Formulary from HB9CL: DLE (german novice amateur radio license) formulary complemented for the HB3 (Novice) OFCOM (Swiss Federal Office of Communications) amateur raido exam.

## License
This work is licensed under CC BY-NC-SA 4.0 

![LICENSE CC BY-NC-SA 4.0](figures/LICENSE.svg "CC BY-NC-SA 4.0")

https://creativecommons.org/licenses/by-nc-sa/4.0/

## Keywords
HB9, HB3, Exam, OFCOM, BAKOM, UFCOM, HB9CL, formulary, novice